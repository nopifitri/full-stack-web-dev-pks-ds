<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::get('/test' , function(){
//     return "ini masuk api.php";
// });
//route ke index PostController
// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');

/*
//proteksi method store, update, delete, pada post dan comment controller jika user blm login
Route::group(['middleware' => ['auth:api']], function () {
    Route::apiResource('post', 'PostController');
    Route::apiResource('role', 'RoleController');
    Route::apiResource('comment', 'CommentController');
});
*/


Route::group([
    'prefix' =>'auth',
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
    Route::post('verifikasi', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');

});
Route::apiResource('post', 'PostController');
Route::apiResource('role', 'RoleController');
Route::apiResource('comment', 'CommentController');


