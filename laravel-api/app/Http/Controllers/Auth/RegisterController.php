<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\RegisterStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd('test masuk register controller');
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($allRequest);

        do {
            $otpCode = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otpCode)->first();
        } while ($check);

        $otp_code = OtpCode::create([
            'otp' => $otpCode,
            'valid_until' => Carbon::now()->addMinutes(15),
            'user_id' => $user->id
        ]);
        //panggil event
        event(new RegisterStoredEvent($otp_code));

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
