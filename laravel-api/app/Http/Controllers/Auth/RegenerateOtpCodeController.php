<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\AuthenticationStoredEvent;
use App\Events\RegenerateOtpCodeEvent;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //  dd('test masuk regenerate controller');
         $allRequest = $request->all();
         $validator = Validator::make($allRequest,[
             'email' => 'required'
         ]);
 
         if($validator->fails()){
             return response()->json($validator->errors(), 400);
         }
 
         $user = User::where('email', $request->email)->first();
 
         if( $user->otpCode){
             $user->otpCode->delete;
         }
     
     
         do {
             $otpCode = mt_rand(100000, 999999);
             $check = OtpCode::where('otp', $otpCode)->first();
         } while ($check);
 
         $otp_code = OtpCode::create([
             'otp' => $otpCode,
             'valid_until' => Carbon::now()->addMinutes(5),
             'user_id' => $user->id
         ]);
 
         //panggil event
        event(new RegenerateOtpCodeEvent($otp_code));
         return response()->json([
             'success' => true,
             'message' => 'Otp Code berhasil digenerate',
             'data' => [
                 'user' => $user,
                 'otp_code' => $otp_code
             ]
         ]);
    }
}
