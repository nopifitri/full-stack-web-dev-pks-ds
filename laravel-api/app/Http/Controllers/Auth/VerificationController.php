<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'otp' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $otp_code = OtpCode::where('otp', $request->otp)->first();
        
        //kalo tidak ada
        if (!$otp_code)
        {
            return response()->json([
                'success' => false,
                'message' => 'Otp Code tidak Ada'
            ], 400);
        }

        //kalo ada tpi sudah lewat waktu valid
        if ( Carbon::now() > $otp_code->valid_until){
            return response()->json([
                'success' => false,
                'message' => 'Otp Code sudah kadaluarsa'
            ], 400);
        }
        //kalo ada dn masih valid
        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);
        $otp_code->delete();
        return response()->json([
            'success' => true,
            'message' => 'Email berhasil diverifikasi',
            'data' => $user
        ], 200);
    }
}
