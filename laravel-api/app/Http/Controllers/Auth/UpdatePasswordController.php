<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd("masuk ke update pass");
        $allRequest = $request->all();
        $validator = Validator::make($allRequest,[
            'email' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $user = User::where('email', $request->email)->first();
        
        //kalo user tidak ada 
        if (!$user){
            return response()->json([
                'success' => false,
                'message' => 'Email tidak ditemukan'
            ], 400);
        }
        //kalo user ada maka update passwordnya
        $user->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password berhasil diubah',
            'data' => $user
        ], 200);
    }
}
