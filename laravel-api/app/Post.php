<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ["title", "description", "user_id"];
    protected $primaryKey = "id";
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating(function ($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            //cara kedua mendapatkan data user yg sdgn login
            $model->user_id = auth()->user()->id;
        });
    }
    //relasi ke tabel comment
    public function comment()
    {
        return $this->hasMany('App\Comments');
    }
    //relasi ke tabel user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
