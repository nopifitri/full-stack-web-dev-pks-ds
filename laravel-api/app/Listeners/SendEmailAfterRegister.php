<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\RegenerateOtpCodeEvent;
use App\Events\RegisterStoredEvent;
use App\Mail\RegisterMail;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailAfterRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticationStoredEvent  $event
     * @return void
     */
    public function handle(RegisterStoredEvent $event)
    {
        Mail::to($event->otpCode->user->email)->send(new RegisterMail($event->otpCode));
    }
}
