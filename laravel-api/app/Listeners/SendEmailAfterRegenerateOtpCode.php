<?php

namespace App\Listeners;

use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\RegenerateOtpCodeEvent;
use App\Mail\RegenerateOtpCodeMail;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailAfterRegenerateOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

        
    }

    /**
     * Handle the event.
     *
     * @param  AuthenticationStoredEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpCodeEvent $event)
    {
        Mail::to($event->otpCode->user->email)->send(new RegenerateOtpCodeMail($event->otpCode));
        
    }
}
