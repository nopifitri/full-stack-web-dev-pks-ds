<?php

namespace App;

use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable = ["username", "email", "name", "role_id", "password", "email_verified_at"];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected static function boot(){
        parent::boot();
        
        static::creating(function ($model){
            if(empty($model->{$model->getKeyName()})){
                $model-> {$model->getKeyName()} = Str::uuid(); 
            }
        });
    }
     //relasi ke tabel otpcode
     public function otpCode()
     {
         return $this->hasOne('App\OtpCode');
     }
     //relasi ke tabel post
     public function post()
     {
        return $this->hasMany('App\Posts');

     }
       //relasi ke tabel comments
       public function comment()
       {
          return $this->hasMany('App\Comments');
  
       }
      // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
